<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kosts', function (Blueprint $table) {
            $table->id();
            $table->integer('owner_id');
            $table->string('name');
            $table->enum('type', ['male', 'female','mix'])->default('mix');
            $table->enum('rent_type', ['monthly', 'yearly'])->default('monthly');
            $table->double('price')->default(0);
            $table->integer('city_id');
            $table->integer('province_id');
            $table->string('long')->nullable();
            $table->string('lat')->nullable();
            $table->text('description')->nullable();
            $table->string('room_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kosts');
    }
}
