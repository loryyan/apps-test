<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_room_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('kost_id');
            $table->integer('user_id');
            $table->integer('owner_id');
            $table->enum('type', ['availability', 'facility', 'discount', 'address'])->default('availability');
            $table->text('note')->nullable();
            $table->text('response')->nullable();
            $table->enum('status', ['open', 'closed'])->default('open');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_room_requests');
    }
}
