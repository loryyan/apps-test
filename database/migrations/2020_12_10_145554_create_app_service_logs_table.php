<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppServiceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_service_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('request_id');
            $table->string('host');
            $table->string('request_type');
            $table->string('type');
            $table->text('header');
            $table->text('request_raw')->nullable();
            $table->text('request_parsed')->nullable();
            $table->text('response_raw')->nullable();
            $table->text('response_parsed')->nullable();
            $table->integer('res_http_code');
            $table->string('res_message')->nullable();
            $table->double('start_time')->default(0);
            $table->double('end_time')->default(0);
            $table->double('duration')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_service_logs');
    }
}
