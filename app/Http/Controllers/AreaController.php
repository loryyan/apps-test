<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Province;

class AreaController extends Controller
{
    public function getProvinces(Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $page_size = $request->get('page_size') ? $request->get('page_size') : 20;

        $offset = ($page - 1) * $page_size;

        $data = Province::select('id', 'name')->get()->skip($offset)->take($page_size);
        $count = Province::all()->count();

        $result = [
            'total_data' => $count, // total all data
            'list' => $data->toArray()
        ];

        return $this->sendResponse($result);
    }

    public function getCities(Request $request)
    {

        $page = $request->get('page') ? $request->get('page') : 1;
        $page_size = $request->get('page_size') ? $request->get('page_size') : 20;

        $offset = ($page - 1) * $page_size;

        $data = City::select('id', 'name', 'province_id')->with(['province' => function($q) {
            $q->select('id', 'name');
        }])->skip($offset)->take($page_size)->get();

        $count = City::all()->count();

        $result = [
            'total_data' => $count, // total all data
            'list' => $data->toArray()
        ];

        return $this->sendResponse($result);
    }
}
