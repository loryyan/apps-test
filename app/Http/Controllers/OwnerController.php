<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kost;
use App\Models\User;
use App\Models\RoomRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE) {
            return $this->sendError('Only Owner Allowed');       
        }
        $opt = ['owner_id' => $user->id];

        $result = Kost::getList($request->get('page'), $request->get('page_size'), $opt);
        return $this->sendResponse($result);

    }

    public function show(Kost $kost)
    {
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE) {
            return $this->sendError('Only Owner Allowed');       
        }

        return $kost;
    }

    public function store()
    {
        $dp = $this->data_post;
        $request = new Request($dp);

        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE) {
            return $this->sendError('Only Owner Allowed');       
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'bail|required|in:male,female,mix',
            'rent_type' => 'bail|required|in:monthly,yearly',
            'price' => 'bail|required',
            'city_id' => 'bail|required',
            'province_id' => 'bail|required',
        ]);
   
        if ($validator->fails()) {
            return $this->sendError('Error Validation', $validator->errors());       
        }
        $data = new Kost();
        $data->owner_id = $user->id;
        $data->name = $request->name;
        $data->type = $request->type;
        $data->rent_type = $request->rent_type;
        $data->price = $request->price;
        $data->city_id = $request->city_id;
        $data->province_id = $request->province_id;
        $data->long = $request->long;
        $data->lat = $request->lat;
        $data->room_size = $request->room_size;
        $data->description = $request->description;

        $save = $data->save();
        if (!$save) {
            return $this->sendError('Request Failed');
        }
        return $this->sendResponse($data);
    }

    
    public function update(Request $request, Kost $kost)
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE || $kost->owner_id != $user->id) {
            return $this->sendError('Only Owner Allowed');       
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'bail|required|in:male,female,mix',
            'rent_type' => 'bail|required|in:monthly,yearly',
            'price' => 'bail|required',
            'city_id' => 'bail|required',
            'province_id' => 'bail|required',
        ]);
   
        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());       
        }

        $kost->update($request->all());
        return $this->sendResponse($kost);
    }

    public function delete(Kost $kost)
    {
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE || $kost->owner_id != $user->id) {
            return $this->sendError('Only Owner Allowed');       
        }

        $kost->delete();
        return $this->sendResponse(null, 'Deleted!');
    }

    public function requestList()
    {
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE) {
            return $this->sendError('Only Owner Allowed');       
        }

        $data = RoomRequest::where(['owner_id' => $user->id, 'status' => 'open'])->get();
        return $this->sendResponse($data);

    }

    public function response(Request $request, RoomRequest $rr)
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        $user = Auth::user();
        $this->user_id = $user->id;
        if ($user->role != User::OWNER_ROLE || $rr->owner_id != $user->id) {
            return $this->sendError('Only Owner Allowed');       
        }

        $validator = Validator::make($request->all(), [
            'response' => 'required',
        ]);
   
        if ($validator->fails()) {
            return $this->sendError('Error Validation', $validator->errors());       
        }

        $rr->response = $request->response;
        $rr->status = 'closed';
        $rr->save();
        
        // TODO:send notif to related user
        return $this->sendResponse(null);
    }
}
