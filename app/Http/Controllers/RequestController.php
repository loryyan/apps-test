<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Kost;
use App\Models\RoomRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class RequestController extends Controller
{
    public function show(User $user)
    {
        return $this->sendResponse($user);
    }

    public function request(Kost $kost)
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        $user = Auth::user();
        $this->user_id = $user->id;
        if (!in_array($user->role, [User::COMMON_ROLE, User::PREMIUM_ROLE])) {
            return $this->sendError('Only User Allowed');       
        }

        $validator = Validator::make($request->all(), [
            'kost_id' => 'required',
            'type' => 'bail|required|in:availability,discount,facility',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Error Validation', $validator->errors());       
        }

        $kost = Kost::find($request->kost_id);
        if (!$kost) {
            return $this->sendError('No Data', [], 404); 
        }
        $rr = new RoomRequest();
        $rr->kost_id = $request->kost_id;
        $rr->user_id = $user->id;
        $rr->owner_id = $kost->owner_id;
        $rr->type = $request->type;
        $rr->note = $request->note;
        $rr->status = 'open';

        if ($rr->save()) {
            $point = User::updatePoint($user, 'db', 5, 'Request '.$request->type);

            if ($point['status'] == true) {

                // TODO:send notif to owner
                return $this->sendResponse(null, 'Request Success, please wait for the owner response');
            } else {
                $rr->delete();
                return $this->sendError($point['msg']);
            }
        }
            
        return $this->sendError('Request Failed');
    }

}
