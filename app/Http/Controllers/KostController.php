<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kost;
use Illuminate\Support\Facades\Validator;

class KostController extends Controller
{
    public function show($kost_id)
    {
        $where[] = ['id', '=', $kost_id];
        $query = Kost::with(['owner' => function($q) {
            $q->select('id', 'name','phone_number', 'email');
        }])->with(['city' => function($q) {
            $q->select('id', 'name');
        }])->with(['province' => function($q) {
            $q->select('id', 'name');
        }])->where($where);

        $kost = $query->get();
        return $this->sendResponse($kost);
    }

    public function search(Request $request)
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        $validator = Validator::make($request->all(), [
            'sort' => 'required|in:price,-price',
            'page' => 'required|integer|min:1',
            'page_size' => 'required|integer|min:1',
        ]);
   
        if ($validator->fails()) {
            return $this->sendError('Error Validation', $validator->errors());       
        }

        $filter = $request->filter;
        $query = [];

        if ($filter['name']) {
            $query[] = ['name', 'like', '%'.$filter['name'].'%'];
        } 

        if ($filter['city_id']) {
            $query[] = ['city_id' ,'=', $filter['city_id']];
        }

        if ($filter['province_id']) {
            $query[] = ['province_id', '=', $filter['province_id']];
        }

        if ($filter['price']) {
            $query[] = ['price' ,'=', $filter['price']];
        } else {
            if ($filter['min_price']) {
                $query[] = ['price', '>=', $filter['min_price']];
            }

            if (($filter['max_price'])) {
                $query[] = ['price', '<=', $filter['max_price']];
            }
        }

        $result = Kost::getList($request->page, $request->page_size, $query, $request->sort);
        return $this->sendResponse($result);
    }
}
