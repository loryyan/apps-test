<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Http\Middleware\FTAes;
use App\Models\AppServiceLog;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* general */
    public $error = 'false';
    public $user_id;
    public $log_data;
    public $response;
    public $is_need_datapost = false;

    /* if set false then using plain data */
    public $use_encrypt_data = false;

    /* headers bank */
    private $headers;

    /* bodies bank */
    public $data_post;
    private $req_raw;
    private $req_parsed;
    private $resp_raw;
    private $resp_parsed;

    /* other data banks */
    private $start_time;
    private $end_time;
    private $secret_key;

    public function callAction($method, $parameters)
    {
        $this->start_time = microtime(true);

        $this->use_encrypt_data = $_ENV['ENCRYPT_MODE'] ? $_ENV['ENCRYPT_MODE'] : 'false';
        $req = new Request();
        $post_data = $req->post();

        if (empty($post_data)) {
            //switch from param to raw json
            $post_data = json_decode(file_get_contents('php://input'), true);
            //end of switch from param to raw json
        }

        $routeArray = app('request')->route()->getAction();
        
        $this->log_data = [
            'request_id' => uniqid(),
            'host' => $this->getUserHostAddress(),
            'request_type' => class_basename($routeArray['controller']),
            'type' => 'incoming',
            'header' => is_array($this->getHeader()) ? json_encode($this->getHeader()) : $this->getHeader(),
            'request_raw' => null,
            'request_parsed' => null,
        ];
        if (in_array($_SERVER['REQUEST_METHOD'], ['POST', 'PUT'])) {
            $this->is_need_datapost = true;
        }

        if($this->is_need_datapost && !isset($post_data['data'])){
            return $this->sendError('Error Validation, data is required');    
        }
        $this->data_post = $this->req_raw = isset($post_data['data']) ? $post_data['data'] : null;
        if (isset($this->req_raw['password'])) {
            unset($this->req_raw['password']);
        }
        if (isset($this->req_raw['confirm_password'])) {
            unset($this->req_raw['confirm_password']);
        }
        $this->log_data['request_raw'] = is_array($this->req_raw) ? json_encode($this->req_raw) : $this->req_raw;
        
        $this->secret_key = $_ENV['SK'];
        // decrypt
        if ($this->is_need_datapost && $this->use_encrypt_data == 'true') {

            if (!$this->secret_key) {
                return $this->sendError('Unable to find secret key');    
            }

            $this->data_post = $this->req_parsed = FTAes::decrypt($this->secret_key, $this->data_post);
            if (!$this->req_parsed) {
                return $this->sendError('Decrypt failed');    
            }
            
            $this->data_post = $arr_check = json_decode($this->req_parsed, true);
            if (!$arr_check) {
                return $this->sendError('Invalid format');    
            }
        }
        // decrypt

        if ($this->use_encrypt_data == 'true') {
            if (isset($this->req_parsed['password'])) {
                unset($this->req_parsed['password']);
            }
            if (isset($this->req_parsed['confirm_password'])) {
                unset($this->req_parsed['confirm_password']);
            }
            $this->log_data['request_parsed'] = is_array($this->req_parsed) ? json_encode($this->req_parsed) : $this->req_parsed;
        }

        return parent::callAction($method, $parameters);
    }

    public function sendResponse($result, $message = 'Success', $code = 200)
    {
        $this->resp_raw = json_encode($result);
        $this->afterAction($result, $message, $code);

        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $this->response,
        ];
        
        return response()->json($response, $code);
    }

    public function sendError($error, $error_msg = [], $code = 400)
    {

        if (!empty($error_msg)) {
            $response['data'] = $error_msg;
            $this->resp_raw = json_encode($error_msg);
            $this->afterAction($error_msg, $error, $code);
        } else {
            $this->afterAction(null, $error, $code);
        }

        $response = [
            'success' => false,
            'message' => $error,
            'data' => $this->response
        ];
        
        return response()->json($response, $code);
    }

    public function afterAction($result, $message, $code)
    {
        if (is_array($result)) {
            $this->resp_raw = json_encode($result);
        }

        
        if ($this->use_encrypt_data == 'true' && $result) {
            $result = $this->resp_parsed = FTAes::encrypt($this->secret_key, $result);
        }

        $this->end_time = microtime(true);   
        $duration = $this->end_time - $this->start_time;

        $res_data = [
            'response_raw' => $this->resp_raw,
            'response_parsed' => $this->resp_parsed,
            'res_http_code' => $code,
            'res_message' => $message,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'duration' => $duration,
            'user_id' => $this->user_id
        ];

        $this->response = $result;

        $this->log_data = array_merge($this->log_data, $res_data);
        AppServiceLog::create($this->log_data);

        // todo: save log
        return true;
    }

    public static function getUserHostAddress()
    {
        $tmp = array();
        foreach (array(/* 'HTTP_CLIENT_IP', */ 'HTTP_X_REAL_IP', /* 'HTTP_X_FORWARDED_FOR', */ 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER)) {
                if (filter_var($_SERVER[$key], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE))
                    return $_SERVER[$key];
                $tmp[] = $_SERVER[$key];
            }
        }
        return reset($tmp);
    }

    public static function getHeader()
    {
        if (!function_exists('getallheaders')) {
            function getallheaders() {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
                return $headers;
            }
        }

        return getallheaders();
    }
}
