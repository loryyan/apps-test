<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\FTAes;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class TestController extends BaseController
{
    public function encrypt(Request $request)
    {
        $data = json_encode($request->all());

        $encrypted = FTAes::encrypt($_ENV['SK'], $data);
        return $encrypted;
    }

    public function decrypt(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'data' => 'required',
        ]);
   
        if ($validator->fails()) {
            return 'Error Validation'. $validator->errors();       
        }

        $decrypted = FTAes::decrypt($_ENV['SK'], $data['data']);
        return $decrypted;
    }


}
