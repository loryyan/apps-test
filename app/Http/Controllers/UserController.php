<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) || 
        Auth::attempt(['phone_number' => $request->phone_number, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;

            $user = User::find($user->id);
            $user->token = $success['token'];
            $user->last_login_attempt = date('Y-m-d H:i:s');
            $user->save();
            return $this->sendResponse($success);

        } else{
            return $this->sendError('Unauthorised', [], 401);
        }
    }

    public function logout(Request $request)
    {
        $logout = $request->user()->token()->revoke();
        if ($logout) {
            return $this->sendResponse(null, 'Successfully logged out');
        }
    }

    public function index(Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $page_size = $request->get('page_size') ? $request->get('page_size') : 20;

        $offset = ($page - 1) * $page_size;

        $data = User::get()->skip($offset)->take($page_size);
        $count = User::all()->count();

        $result = [
            'total_data' => $count, // total all data
            'list' => $data->toArray()
        ];

        return $this->sendResponse($result);
    }

    public function show(User $user)
    {
        return $this->sendResponse($user);
    }

    public function store()
    {
        $dp = $this->data_post;
        $request = new Request($dp);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'bail|required|email|unique:users',
            'phone_number' => 'bail|required|unique:users|min:10|max:14',
            'password' => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@$!%*#?&.]).*$/',
            'confirm_password' => 'bail|required|same:password',
            'role' => 'required|in:owner,user_common,user_premium',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Error Validation', $validator->errors());       
        }
            
        $user = User::register($request->all());
        if (!$user) {
            return $this->sendError('Request Failed');
        }

        if ($request->role == User::COMMON_ROLE) {
            $amount = 20;
        } elseif ($request->role == User::PREMIUM_ROLE) {
            $amount = 40;
        } else {
            $amount = 0;
        }

        User::updatePoint($user, 'cr', $amount, 'Register user '. $request->role);

        return $this->sendResponse($user);
    }

}
