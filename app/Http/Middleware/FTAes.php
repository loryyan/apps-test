<?php

namespace App\Http\Middleware;

class FTAes
{
    const AES_256 = "aes-256-ctr";

    public static function encrypt($key, $data)
    {
        try {
            $ivlen = openssl_cipher_iv_length(self::AES_256);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext_raw = openssl_encrypt($data, self::AES_256, $key, $options = OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
            $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

            return $ciphertext;    

        } catch (\Exception $e) {
            return false;
        }
        
    }

    public static function decrypt($key, $data)
    {
        try {
            $c = base64_decode($data);
            $ivlen = openssl_cipher_iv_length(self::AES_256);
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len = 32);
            $ciphertext_raw = substr($c, $ivlen + $sha2len);
            $original_plaintext = openssl_decrypt($ciphertext_raw, self::AES_256, $key, $options = OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);

            if (hash_equals($hmac, $calcmac)) { //PHP 5.6+ timing attack safe comparison
                return $original_plaintext;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

}