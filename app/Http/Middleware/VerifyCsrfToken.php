<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/user/register',
        '/user/register',
        '/kost/search',
        '/owner/create-kost',
        '/owner/update-kost/*',
        '/owner/delete-kost/*',
        '/owner/response-request/*',
        '/kost-request',
        '/login',
        '/logout',
        '/encrypt',
        '/decrypt',
    ];
}
