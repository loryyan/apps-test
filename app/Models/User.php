<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use App\Models\UserPointLog as Point;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory;

    const OWNER_ROLE = 'owner';
    const COMMON_ROLE = 'user_common';
    const PREMIUM_ROLE = 'user_premium';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone_number',
        'email',
        'password',
        'role',
        'point',
        'token',
        'last_login_attempt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public static function register($request)
    {
        $request['password'] = Hash::make($request['password']);
        $request['created_at'] = date('Y-m-d H:i:s');

        return User::create($request);
    }

    public static function updatePoint(User $user, $type, $amount, $desc = null)
    {
        if ($type == 'cr') {
            $user->point = $user->point + $amount;
            $desc = $desc ? $desc : 'Credit';
        } elseif ($type == 'db') {
            $user->point = $user->point - $amount;
            $desc = $desc ? $desc : 'Debit';
        }

        if ($user->point < 0) {
            return ['status' => false, 'msg' => 'Insufficient point'];
        }

        $pl = new Point();
        $pl->user_id = $user->id;
        $pl->type = $type;
        $pl->description = $desc;
        $pl->point = $amount;


        if ($user->save() && $pl->save()) {
            return ['status' => true, 'msg' => 'Request Failed'];
        }

        return ['status' => false, 'msg' => 'Request Failed'];

    }

    public static function rechargePoint()
    {
        $data = User::all();
        foreach ($data as $key => $value) {
            self::updatePoint($value, 'cr', 10, 'Monthly recharge');
        }

        return true;
    }
}
