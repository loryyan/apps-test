<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{
    use HasFactory;

    public $owner;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id',
        'name',
        'type',
        'rent_type',
        'price',
        'city_id',
        'province_id',
        'long',
        'lat',
        'room_size',
        'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function owner() 
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }
    public function city() 
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
    public function province() 
    {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }

    public static function getList($page, $page_size, $option = null, $order = null) 
    {
        $page = $page ? $page : 1;
        $page_size = $page_size ? $page_size : 20;
        $offset = ($page - 1) * $page_size;

        $query = Kost::select('id', 'name', 'type', 'price', 'owner_id')->with(['owner' => function($q) {
            $q->select('id', 'name','phone_number', 'email');
        }]);

        if ($option) {
            $query = $query->where($option);
        }
        
        if ($order == 'price') {
            $query->orderBy('price', 'asc');
        } elseif ($order == '-price') {
            $query->orderBy('price', 'desc');
        }

        $query = $query->get();
        $data = $query->skip($offset)->take($page_size);
        
        $count = $query->count();

        $result = [
            'total_data' => $count, // total all data
            'list' => $data->toArray()
        ];

        return $result;
    } 

}
