<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppServiceLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'request_id',
        'host',
        'request_type',
        'type',
        'header',
        'request_raw',
        'request_parsed',
        'response_raw',
        'response_parsed',
        'res_http_code',
        'res_message',
        'start_time',
        'end_time',
        'duration'
    ];
}
