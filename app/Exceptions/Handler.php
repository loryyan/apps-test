<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\RequestException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['success' => false, 'message' => 'Unauthenticated!', 'data' => null], 401);
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            return response()->json(['success' => false, 'message' => 'Not Found!', 'data' => null], 404);
        } elseif ($exception instanceof ModelNotFoundException) {
            return response()->json(['success' => false, 'message' => 'Not Found!', 'data' => null], 404);
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(['success' => false, 'message' => $exception->getMessage(), 'data' => null], 405);
        }

        return parent::render($request, $exception);
    }
}
