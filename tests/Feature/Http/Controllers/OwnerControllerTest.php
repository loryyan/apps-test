<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\User;

class OwnerControllerTest extends TestCase
{
    use WithFaker;
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateKost()
    {
        $type = ['male', 'female', 'mix'];
        $rent_type = ['yearly', 'monthly'];

        $user = User::factory()->create(['role' => 'owner']);
        $data['data'] = [
            'name' => 'Kost '.$this->faker->name,
            'type' => $type[rand(0,2)],
            'rent_type' => $rent_type[rand(0,1)],
            'price' => $this->faker->randomNumber(),
            'city_id' => rand(223,227),
            'province_id' => '14',
            'room_size' => '3x5m',
            'description' => $this->faker->text(100),
        ];
        $response = $this->actingAs($user)->post('/owner/create-kost', $data);
        $response->assertStatus(200);
    }
}
