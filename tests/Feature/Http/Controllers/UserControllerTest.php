<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\User;

class UserControllerTest extends TestCase
{
    use WithFaker;
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $roles = ['owner', 'user_premium', 'user_common'];

        $data['data'] = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'phone_number' => preg_replace("/[^0-9]/", "", $this->faker->phoneNumber),
            'password' => 'Paswd!23', // password
            'confirm_password' => 'Paswd!23',
            'role' => $roles[rand(0,2)],
        ];
        echo json_encode($data);

        $response = $this->post('/user/register', $data);
        $response->assertStatus(200);
    }

    public function testUserList()
    {
        $page = rand(1,5);
        $page_size = rand(1,10);
        $uri = '/user/list?page='.$page.'&page_size='.$page_size;
        $response = $this->get($uri);

        $response->assertStatus(200);
    }

    public function testUserDetail()
    {
        $list = User::select('id')->get();
        $limit = (count($list)-1) < 0 ? 0 : (count($list)-1);
        $id = $list[rand(0,$limit)]['id'];
        $response = $this->get('/user/'.$id);

        $response->assertStatus(200);
    }
}
