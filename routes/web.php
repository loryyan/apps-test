<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return json_encode(['status' => 111, 'message' => 'Apps API']);
    // return view('welcome');
});

// User routes
Route::post('login', ['as' => 'login', 'uses' => 'App\Http\Controllers\UserController@login']);

Route::get('user/list', 'App\Http\Controllers\UserController@index');
Route::get('user/{user}', 'App\Http\Controllers\UserController@show');
Route::post('user/register', 'App\Http\Controllers\UserController@store');

// Kost Routes
Route::post('kost/search', 'App\Http\Controllers\KostController@search');
Route::get('kost/{kost}', 'App\Http\Controllers\KostController@show');

// Area Routes
Route::get('area/city-list', 'App\Http\Controllers\AreaController@getCities');
Route::get('area/province-list', 'App\Http\Controllers\AreaController@getProvinces');

// Need Auth ---
Route::group(['middleware' => 'auth:api'], function(){
    // Owner Routes
    Route::get('owner/kost-list', 'App\Http\Controllers\OwnerController@index');
    Route::get('owner/kost/{kost}', 'App\Http\Controllers\OwnerController@show');
    Route::post('owner/create-kost', 'App\Http\Controllers\OwnerController@store');
    Route::put('owner/update-kost/{kost}', 'App\Http\Controllers\OwnerController@update');
    Route::delete('owner/delete-kost/{kost}', 'App\Http\Controllers\OwnerController@delete');
    Route::get('owner/request-list', 'App\Http\Controllers\OwnerController@requestList');
    Route::post('owner/response-request/{rr}', 'App\Http\Controllers\OwnerController@response');

    // Request Route
    Route::post('kost-request', 'App\Http\Controllers\RequestController@request');

    Route::get('logout', 'App\Http\Controllers\UserController@logout');

});
// ---


// Helper :
Route::post('encrypt', 'App\Http\Controllers\TestController@encrypt');
Route::post('decrypt', 'App\Http\Controllers\TestController@decrypt');